cronwhen
========

cronwhen reports the next-run(s) of crontab entries.

Download from <http://bitbucket.org/beneford/cronwhen>  
	Use the source tab, select cronwhen and download the raw format

Licensed under GPL - see <http://www.gnu.org/licenses/gpl.html>

### Change log

[comment]: <> (double-space at end of line forces line break)
V1.01 - allow leading 0s on time specifiers  
V1.00 - initial release  
