#
# spec file for package 
#
# Copyright (c) 2014 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

Name:           cronwhen
Version:	1.01
Release:	1
License:	GPL
Summary:	List the next run for items in crontab
Url:		https://bitbucket.org/beneford/cronwhen
Group:		System/Daemons
Source:		%{name}-%{version}.tgz
Requires:	bash
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildArch:	noarch

%description
This program lists the crontab jobs and when they will next run.

%prep
%setup

%build

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
install -m 755 %{name} ${RPM_BUILD_ROOT}%{_bindir}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}
install -m 755 -D %{name}.man ${RPM_BUILD_ROOT}%{_mandir}/man1/%{name}.1
gzip ${RPM_BUILD_ROOT}%{_mandir}/man1/%{name}.1
chmod 755 ${RPM_BUILD_ROOT}%{_mandir}/man1/%{name}.1.gz

%clean

%post -p /usr/bin/mandb

%files
%defattr(-,root,root)
%doc LICENSE
%{_bindir}/*
%{_mandir}/*

%changelog
