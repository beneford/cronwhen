.\" Manpage for cronwhen.
.TH man 8 "25 Apr 2014" "1.0" "cronwhen man page"
.SH NAME
cronwhen \- list next-run times for crontab jobs
.SH SYNOPSIS
cronwhen [options] [cron-lines-to-test]
.SH DESCRIPTION
show the next time(s) cron jobs will be run in the next year
cron-lines-to-test is a quoted list of cron lines: "12 30 * * * Command"
.SH OPTIONS
.nf
\--help        show this help screen
\--user name   use crontab for named user
--count n      n is the number of future events to show (default=2)
--now date     assume the current time is 'date' (format as for date command)'
--version      show version
--debug        show debug information (repeat for more)"

.fi
short options use the initial letter of long options:
.nf
-h -u name -c n --n date -v -d
.fi
.SH BUGS
No known bugs.
.SH AUTHOR
Raymond Obin (raymond.obin@beneford.com)
