SCRIPT=cronwhen
MAN=cronwhen.man

PREFIX      = /usr/share
BINDIR      = $(PREFIX)/bin
MANINDEX    = 1
MANDIR      = $(PREFIX)/man/man$(MANINDEX)

all:
	# Please use make install

uninstall:
	rm -f $(BINDIR)/$(SCRIPT)
	rm -f $(MANDIR)/$(MAN).$(MANINDEX)*

install:
	cp -p $(SCRIPT) $(BINDIR)/$(SCRIPT)
	gzip -c $(MAN) > $(MANDIR)/$(SCRIPT).$(MANINDEX).gz
	mandb > /dev/null
